## CFS Redesign

CFS Redesign Frontend Project

## Setup

Make sure bundler is installed

  gem install bundler

**Note:** Mac OSX users may need to run commands as SUDO

Navigate to the root folder of the project.

Install required gems using bundler

  bundle install

We use [Grunt](http://gruntjs.com/) to automate all front-end tasks (local dev and production).

First we need to download all the projects grunt dependancies using:

    npm install

Next, Copy **localdev.json.default** into a new **localdev.json** file.

Now run the following from your CLI (e.g Terminal):

    bundle exec grunt

This will:

- Compile SASS to CSS
- Concat Javascript
- Watch for changes to SASS/JS and recompile

Running the grunt command should be all you need to start working on the project!

## localdev.json

The localdev.json file should look like

    {
      "css": "styles",
      "js": "script",
      "stagingFolder": ""
    }


The properties translate to the following:

- css: The name of the css file that is output into /build/css.
- css: The name of the js file that is output into /build/js.
- stagingFolder: The path to custom folder for the project on dublin or capetown. When you save a SASS file, the compiled css will be copied to the staging folder automatically using grunt. You can set this to "build/staging" if you don't want to automatically deploy.

## Font Icons

We are using [Icomoon](http://iconmoon.io/) to create a font icon set all our icons.

The icon set can be accessed in the /fonts/icomoon directory.

- demo.html - An overview of all the font icons currently in the set
- selection.json - Use this to add more icons to this set if required on icomoon.io

On the staging/production site, the icomoon folder needs to be in /custom/fonts/icomoon

## Placeholder Images

All placeholder images can be saved in /files

## Production Build

Simply run the followin from your CLI (e.g Terminal):

  bundle exec grunt build

This will:

- Concat & Minify Javascript + CSS

## Components
description of components for this project

## Sections
description of sections for this project