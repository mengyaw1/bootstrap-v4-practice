function setupLocalDev () {
  var shell = require('shelljs');

  if (!shell.test('-e', 'localdev.json')) {
    shell.cp('.localdev.json.default','localdev.json');
    console.log('Created localdev.json');
  }
}

module.exports = function(grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);
  // load all grunt tasks just-in-time
  require('jit-grunt')(grunt);

  // main config
  grunt.initConfig({

    task: {
      target: {
        src: setupLocalDev()
      }
    },

    pkg: grunt.file.readJSON('package.json'),
    localdev: grunt.file.readJSON('localdev.json'),

    project: {
      cssOut: '<%= localdev.css %>',
      jsOut: '<%= localdev.js %>',
      stagingFolder: '<%= localdev.stagingFolder %>',
      js: ['app/js/script.js']
    },

    /**
     * Project banner
     * Dynamically appended to CSS/JS files
     * Inherits text from package.json
     */
    tag: {
      banner: '/*!\n' +
              ' * <%= pkg.name %>\n' +
              ' * <%= pkg.title %>\n' +
              ' * @version <%= pkg.version %>\n' +
              ' * @created <%= grunt.template.today("yyyy/mm/dd") %>\n' +
              ' * ** This file was compiled with Grunt. Please do not edit it directly. **\n' +
              ' */\n'
    },

    sass: {
      dev: {
        options: {
          style: 'expanded',
          compass: true,
          banner: '<%= tag.banner %>'
        },
        files: {
          'build/custom/styles/<%= project.cssOut %>.css':'app/sass/style.scss'
        }
      },
      dist: {
        options: {
          style: 'compressed',
          compass: true,
          banner: '<%= tag.banner %>'
        },
        files: {
          'build/custom/styles/<%= project.cssOut %>.css':'app/sass/style.scss',
        }
      }
    },

    autoprefixer: {
      main: {
        src: 'build/custom/styles/<%= project.cssOut %>.css',
        options: {
          browsers: ['last 2 versions', 'ie >= 8', '> 1%', 'iOS >= 6']
        }
      }
    },

    copy: {
      js: {
        expand: true,
        cwd: 'app/js',
        src: ['**'],
        dest: 'build/custom/js/'
      },
      fonts: {
        expand: true,
        cwd: 'app/fonts',
        src: ['**'],
        dest: 'build/custom/fonts/'
      },
      templates: {
        expand: true,
        cwd: 'app/templates',
        src: ['**'],
        dest: 'build/custom/templates/'
      },
      velocity: {
        expand: true,
        cwd: 'app/velocity',
        src: ['**'],
        dest: 'build/velocity/'
      },
      img: {
        expand: true,
        cwd: 'app/img',
        src: ['**'],
        dest: 'build/custom/files/media/'
      }
    },

    imagemin: {
      options: {
        //optimizationLevel: 7
      },
      main: {
        files: [{
          expand: true,
          cwd: 'app/img',
          src: '**/*.{png,jpg,jpeg,gif,svg}',
          dest: 'build/custom/files/media/'
        }]
      }
    },

    uglify: {
      dev: {
        options: {
          banner: '<%= tag.banner %>',
          mangle: false,
          compress: false,
          beautify: true,
          report: false,
          preserveComments: 'all'
        },
        files: {
          'build/custom/js/<%= project.jsOut %>.min.js': '<%= project.js %>'
        }
      },
      prod: {
        options: {
          banner: '<%= tag.banner %>',
          mangle: true,
          compress: {},
          beautify: false,
          preserveComments: false
        },
        files: {
          'build/custom/js/<%= project.jsOut %>.min.js': '<%= project.js %>'
        }
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: 'app/js/script.js'
    },

    jscs: {
      src: 'app/js/script.js',
      options: {
        config: '.jscsrc'
      }
    },

    watch: {

      js: {
        files: 'app/js/{,*/}*.js',
        tasks: ['jshint', 'jscs', 'uglify:dev', 'copy:js']
      },

      sass: {
        files: 'app/sass/{,*/}*.{scss,sass}',
        tasks: ['sass:dev', 'autoprefixer']
      },

      fonts: {
        files: ['app/fonts/**/*'],
        tasks: ['copy:fonts']
      },

      templates: {
        files: ['app/templates/**/*'],
        tasks: ['copy:templates']
      },

      velocity: {
        files: ['app/velocity/**/*'],
        tasks: ['copy:velocity']
      },

      img: {
        files: ['app/img/**/*'],
        tasks: ['copy:img']
      }

    }
  });

  // Dev task
  grunt.registerTask('default', [
    'jshint',
    'jscs',
    'sass:dev',
    'autoprefixer',
    'uglify:dev',
    'copy',
    'watch'
  ]);

  // Production Build task
  grunt.registerTask('build', [
    'sass:dist',
    'autoprefixer',
    'uglify:prod',
    'copy'
  ]);
};
