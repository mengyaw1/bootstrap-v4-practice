// global _:false, moment:false, Modernizr:false

var CFS = window.CFS || {};

;(function($) {
  'use strict';

  CFS.breakpoints = {
    'smallMax': 767,
    'mediumMin': 768,
    'mediumMax': 1039,
    'largeMin': 1040,
    'largeMax': 1679,
    'hugeMin': 1680
  };

  CFS.init = function() {
    console.log('CFS');
    CFS.nav.init();
  };

  // Nav function
  CFS.nav = {
    init: function() {
      // Do not run if there's no nav
      if (!$('nav').length) {
        return false;
      }

      console.log('NAV');
      var self = this;
      var viewport = CFS.utils.viewport();

      self.initSectionNav();
      if (viewport.width < CFS.breakpoints.largeMin) {
        self.initMobileNav();
      } else {
        self.initNav();
      }

      $(window).on('orientationchange', function() {
        viewport = CFS.utils.viewport();

        if (viewport.width === CFS.breakpoints.mediumMin) {
          self.resetMobileNav();
        } else {
          self.resetDesktopNav();
        }
      });
    },
    initNav: function() {
      var self = this;
      var $overlayTargets = $('body, nav');
      var $header = $('header');
      var $body = $('body');

      $('.js-toggle-nav-child').on('click', function(event) {
        event.preventDefault();

        var $this = $(this);
        var $target = $($this.attr('href'));
        var $parent = $this.parent();

        if ($parent.hasClass('child--open')) {
          $parent.removeClass('child--open');
          $target.fadeOut();
          $overlayTargets.removeClass('nav--open');
        } else {
          $('.child--open').removeClass('child--open');
          $parent.addClass('child--open');
          $('.nav__child').not($target).fadeOut();
          $target.fadeIn();
          $overlayTargets.addClass('nav--open');
        }
      });

      $('.js-close-nav').on('click', function() {
        var $this = $(this);
        var $target = $this.parent().prev('.js-toggle-nav-child');

        $target.trigger('click');
      });

      // Sticky nav for desktop
      var subNavTop;
      if ($('.section-nav').length > 0) {
        subNavTop = $('.section-nav').offset().top - 100; // deduct sticky nav height in order to trigger sticky for section nav when sticky nav bottom touches section nav
      }
      var navTop = $header.outerHeight();
      self.updateSticky($body, navTop, subNavTop);

      $(window).on('scroll', function() {
        self.updateSticky($body, navTop, subNavTop);
      });
    },
    initMobileNav: function() {
      $(window).unbind('scroll');

      $('.js-toggle-nav').on('click', function() {
        $(this).toggleClass('nav--open');
        $('body').toggleClass('nav--open');
        $('nav > .row').slideToggle();
      });

      $('.js-toggle-nav-child').on('click', function(event) {
        event.preventDefault();

        var $this = $(this);
        var $target = $($this.attr('href'));
        var $parent = $this.parent().parent('ul');

        $parent.addClass('child--open');

        // 300 is required to be consistant with css transitino setting.
        $target.animate({
          left: '0'
        }, 300, 'linear');
      });

      $('.js-toggle-nav-parent').on('click', function(event) {
        event.preventDefault();

        var $this = $(this);
        var $target = $this.parent().parent('ul');
        var $parent = $($this.attr('href')).parent('ul');

        $parent.removeClass('child--open');

        // 300 is required to be consistant with css transitino setting.
        $target.animate({
          left: '100%'
        }, 300, 'linear');
      });
    },
    initSectionNav: function() {
      $('.section-nav a').on('click', function(event) {
        event.preventDefault();

        CFS.utils.scrollToElement($($(this).attr('href')));
      });
    },
    updateSticky: function($target, navPos, subNavTop) {
      if ($(window).scrollTop() >= navPos && !$target.hasClass('is-sticky')) {
        $target.addClass('is-sticky');
      } else if ($(window).scrollTop() < navPos && $target.hasClass('is-sticky')) {
        $target.removeClass('is-sticky');
      }

      if ($('.section-nav').length > 0) {
        if ($(window).scrollTop() >= subNavTop) {
          $target.addClass('is-sticky is-sticky-section');
        } else if ($(window).scrollTop() < subNavTop && $target.hasClass('is-sticky-section')) {
          $target.removeClass('is-sticky-section');
        }
      }
    },
    resetDesktopNav: function() {
      var self = this;

      $('.nav--open').removeClass('nav--open');
      $('nav > .row, .nav__child').attr('style', '');
      $('.child--open').removeClass('child--open');
      $('.js-toggle-nav-parent, .js-toggle-nav-child, .js-toggle-nav').off('click');

      self.initNav();
    },
    resetMobileNav: function() {
      var self = this;

      $('.child--open').removeClass('child--open');
      $('.nav--open').removeClass('nav--open');
      $('.js-toggle-nav-child, .js-close-nav').off('click');

      self.initMobileNav();
    }
  };

  // Helper utils
  CFS.utils = {
    // return viewport width / height (better cross-browser)
    viewport: function() {
      var e = window;
      var a = 'inner';
      if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
      }

      return {width: e[a + 'Width'], height: e[a + 'Height']};
    },
    scrollToElement: function($target, duration) {
      var scrollTo = 0;

      if (!duration) {
        duration = 500;
      }

      if ($target) {
        scrollTo = $target.offset().top;
      }

      $('html, body').animate({
        scrollTop: scrollTo
      }, duration);
    },
    debounce: function(func, wait, immediate) {
      var timeout;

      return function() {
        var context = this;
        var args = arguments;

        var later = function() {
          timeout = null;
          if (!immediate) {
            func.apply(context, args);
          }
        };

        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
          func.apply(context, args);
        }
      };
    }
  };

  CFS.init();

})(jQuery);
